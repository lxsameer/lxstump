;; swank.lisp -*- Mode: Lisp -*-
(in-package :stumpwm)

(setf *debug-level* 3)
(set-module-dir "/home/lxsameer/src/swm/")

;; SBCL
;; (ql:quickload :clx)
;; (ql:quickload :cl-ppcre)
;; (ql:quickload :alexandria)
;; (ql:quickload :swank)
(ql:quickload :clx-truetype)
;; (xft:cache-fonts)
(ql:quickload :xml-emitter)
(ql:quickload :dbus)


;; Notification System
(load-module "notify")
(notify:notify-server-toggle)

(defun notification-handler (app icon summary body)
  "Does things with incoming notifications"
  (message summary))

(setf notify:*notification-received-hook* '(notification-handler))

;; THEME ----------------------------------------------------------------------
;; Clone the repo of cl-base16 in ~/quicklisp/local-projects/
(ql:quickload :cl-base16)
;; (cl-base16:update)
(load-module :stumpwm-base16)
(stumpwm-base16:load-theme "material" "materialtheme")

;; FONT
(load-module "ttf-fonts")
(set-font (make-instance 'xft:font :family "Hack" :subfamily "Regular" :size 12))


;; Load swank
(message "Running swank server...")
(require 'swank)
(defvar *swank-running* nil)
(unless *swank-running*
  (swank:create-server :port 4004
                       :style swank:*communication-style*
                       :dont-close t)
  (echo-string (current-screen) "Starting swank. M-x slime-connect RET 4004")
  (setf *swank-running* t))


(message "Setting up keybindings and global config...")
(set-prefix-key (kbd "C-q"))
(setf *mode-line-border-width* 1)

;; width in pixels given to the borders of windows with maxsize or ratio
;; hints
(setf *maxsize-border-width* 1)

;; width in pixels given to the borders of transient or pop-up windows
(setf *transient-border-width* 1)

;; width in pixels given to the borders of regular windows
(setf *normal-border-width* 1)

(setf *window-border-style* :thin)

;; ;; set the border color for focused windows
;; (set-focus-color "orange")

;; (set-unfocus-color "palegreen3")
;; Use the current shell of the system for shell commands
(setq *shell-program* (stumpwm::getenv "SHELL"))



(toggle-mode-line (current-screen) (current-head))

(define-key *root-map* (kbd "v") "vsplit")
(define-key *root-map* (kbd "h") "hsplit")

(define-key *root-map* (kbd "c") "exec st")
(define-key *root-map* (kbd "e") "exec fg42")


(defcommand translate-clipboard () ()
  "Translate clipboard to EN."
  (run-shell-command
   "notify-send -t 0 --icon=info \"$(xsel -b -o | translate-shell -b)\""))

(define-key *top-map* (kbd "s-t") "translate-clipboard")
